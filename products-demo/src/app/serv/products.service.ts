import { Injectable } from '@angular/core';
import { Product } from '../model/product';

@Injectable({ //  this is a singlton class 
  //- it is created by the framework and passed to components within constuctor
  providedIn: 'root'
})
export class ProductsService {

  productList:Product[] = []
  
  saveProduct(newProduct:Product){
    this.productList.push(newProduct)
  }

  search(productName:string){
    for (let product of this.productList){
      if(product.name == productName){
        return product
      }
    }
    return null
  }
  constructor() { }
}
