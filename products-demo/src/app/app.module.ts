import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { SearchComponent } from './comp/search/search.component';
import { CreateComponent } from './comp/create/create.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { Routes } from '@angular/router';
import { ListComponent } from './comp/list/list.component';

export const routes: Routes = [
  { path: 'create', pathMatch: 'full', component: CreateComponent },
  { path: 'search', pathMatch: 'full', component: SearchComponent },
  { path: 'list', pathMatch: 'full', component: ListComponent },
]


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    CreateComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
