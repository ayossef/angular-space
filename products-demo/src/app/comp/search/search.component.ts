import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductsService } from 'src/app/serv/products.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  service:ProductsService
  constructor(private _productService:ProductsService) {
    this.service = _productService
  }

  outputMsg:string = "Product Not Found"
  foundFlag:boolean = false
  product:Product = new Product("",0,"")
  searchName:string = ""


  search(){
    var result = this.service.search(this.searchName)
    if (result == null){
      console.log("Not Found - "+result)
      this.foundFlag = false
    }
    else{
      console.log("Found - "+result)
      this.product = result
      this.foundFlag = true
    }
    // call search function search
    // if result was null 
      // -> set flag to false
    // else 
      // -> set flag to true
      // -> set the values of the product


  }

  ngOnInit(): void {
  }

}
