import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductsService } from 'src/app/serv/products.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  service:ProductsService
  constructor(private _prodcutService:ProductsService) {
    this.service = _prodcutService
  }


  name:string = ""
  price:number = 0
  imgUrl:string = ""

  save(){
    // call save method from the service
    var product = new Product(this.name, this.price, this.imgUrl)
    this.service.saveProduct(product)
  }
  ngOnInit(): void {
  }

}
