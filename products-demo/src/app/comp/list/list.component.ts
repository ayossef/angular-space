import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductsService } from 'src/app/serv/products.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  productsList:Product[] = []
  constructor(private _productsServerice:ProductsService) { 
    this.productsList = _productsServerice.productList
  }

  ngOnInit(): void {
  }

}
