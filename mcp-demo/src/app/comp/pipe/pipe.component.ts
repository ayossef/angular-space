import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe',
  templateUrl: './pipe.component.html',
  styleUrls: ['./pipe.component.scss']
})
export class PipeComponent implements OnInit {

  price = 10.39
  title = "WELcome"
  options = {
    "flag1":"value1",
    "flag2":true
  }
  list = ['MZ','UK','ZA','EG','AE']
  constructor() { }

  ngOnInit(): void {
  }

  valiedate(event:Event){
    this.price = +(event.target as HTMLInputElement).value
    console.log('validating ..'+this.price)
    if(this.price > 100){
      console.log('resetting price to zero')
      this.price = 0
    }
  }
}
