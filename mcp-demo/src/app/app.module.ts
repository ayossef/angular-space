import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { PipeComponent } from './comp/pipe/pipe.component';


import { Routes } from '@angular/router';
import { AccountComponent } from './comp/account/account.component';
import { LoginComponent } from './comp/login/login.component';
import { RegisterComponent } from './comp/register/register.component';
import { ProductsComponent } from './comp/products/products.component';
import { ListComponent } from './comp/list/list.component';
import { DetailsComponent } from './comp/details/details.component';
import { EditComponent } from './comp/edit/edit.component';
import { SocialComponent } from './comp/social/social.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponent } from './comp/material/material.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider'; 

export const routes: Routes = [
  {path:'pipes', component:PipeComponent},
  {path:'products', component:ProductsComponent,children:[
    {path:'list', component:ListComponent},
    {path:'details/:id', component:DetailsComponent},
    {path:'edit/:id', component:EditComponent},
  ]},
  {path:'account', component:AccountComponent, children:[
    {path:'login', component:LoginComponent,children:[
      {path:'social', component:SocialComponent},
    ]},
    {path:'register', component:RegisterComponent},
  ]},
]

@NgModule({
  declarations: [
    AppComponent,
    PipeComponent,
    AccountComponent,
    LoginComponent,
    RegisterComponent,
    ProductsComponent,
    ListComponent,
    DetailsComponent,
    EditComponent,
    SocialComponent,
    MaterialComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
