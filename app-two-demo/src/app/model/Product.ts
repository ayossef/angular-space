export class Product {
    name: string
    price: number
    inStock: boolean
    quantity: number
    imgUrl: string
    constructor(name:string, price:number, inStock:boolean, quantity:number, imgUrl:string) {
        this.name = name
        this.price = price
        this.inStock = inStock
        this.quantity = quantity
        this.imgUrl = imgUrl
    }
}