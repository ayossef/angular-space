import { Injectable } from '@angular/core';
import { Product } from '../model/Product';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  private products: Product[] = [];
  constructor() {
      this.products.push(new Product("iPhone",1000,true,100,""))
      this.products.push(new Product("mouse",50,true,10000,""))
      this.products.push(new Product("MacBook Pro",4000,true,20,""))
      this.products.push(new Product("AirPods",700,true,250,""))
   }
   getList(){
     return this.products
   }
   addItem(product:Product){
     this.products.push(product)
   }
}
