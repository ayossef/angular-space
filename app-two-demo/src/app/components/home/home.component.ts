import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  maxValue:number = 10
  decDisabled: boolean = true
  incDisabled: boolean = false
  counter: number = 0
  username: string = ""
  constructor() { }

  inputEdit(kue:Event){
    this.username= (kue.target as HTMLInputElement).value
    console.log('Input Edit is called')
  }
  inc(){
    this.counter +=1
    if(this.counter == this.maxValue){
      this.incDisabled = true
    }
    var msg = "hello"
    console.log(msg)
    
    this.decDisabled = false
  }

  decrement(){
    this.incDisabled = false
    if(this.counter > 0){
      this.counter -=1  
    }
    if(this.counter == 0){
      this.decDisabled = true
    }
  }
  ngOnInit(): void {
  }

}
