import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/Product';
import { DataServiceService } from 'src/app/services/data-service.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  prName:string = "" 
  quantity:number = 0
  price:number = 0
  dataServ:DataServiceService 
  constructor(dataServ:DataServiceService) {
    this.dataServ = dataServ
  }

  ngOnInit(): void {
  }

  save(){
    this.dataServ.addItem(new Product(this.prName, this.price, true,this.quantity,""))
  }

}
