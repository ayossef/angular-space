import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-twoways',
  templateUrl: './twoways.component.html',
  styleUrls: ['./twoways.component.css']
})
export class TwowaysComponent implements OnInit {

  username:string = ""
  displayName:string = ""
  constructor() { }

  ngOnInit(): void {
  }

  display(){
    this.displayName = this.username
  }
  clear(){
    this.username = ""
    this.displayName = ""
  }

}
