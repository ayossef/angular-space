import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TwowaysComponent } from './twoways.component';

describe('TwowaysComponent', () => {
  let component: TwowaysComponent;
  let fixture: ComponentFixture<TwowaysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TwowaysComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TwowaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
