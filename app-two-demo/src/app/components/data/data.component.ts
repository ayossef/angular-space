import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/Product';
import { DataServiceService } from 'src/app/services/data-service.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  products:Product[]
  constructor(dataServ:DataServiceService) { 
    this.products = dataServ.getList()
  }
  ngOnInit(): void {
    
  }

}
