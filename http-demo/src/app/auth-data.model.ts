export class AuthData {
    isLoggedIn:boolean = false  // authntication
    roles:string[] = [] // authorization
    username:string = ""
    activeStatus:boolean = false
}
