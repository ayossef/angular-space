import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/model/product.model';
import { ProductsApiService } from 'src/app/srv/products-api.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.sass']
})
export class DetailsComponent implements OnInit {

  id:number = 0
  currentItem!:Product
  constructor(private router:ActivatedRoute, private apiService:ProductsApiService) {
      this.id = router.snapshot.params['id']
   }

  ngOnInit(): void {
    this.apiService.loadItem(this.id).subscribe(item => this.currentItem = item)
  }

  editItem(){
    this.currentItem.price -= 10
    this.apiService.update(this.currentItem,this.id)
  }

}
