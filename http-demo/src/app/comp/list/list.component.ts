import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product.model';
import { HttpClient } from '@angular/common/http';
import { ProductsApiService } from 'src/app/srv/products-api.service';
import { AuthService } from 'src/app/srv/auth.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  list:Product[] = []
  output:string = ""
  auth!:AuthService
  constructor(private api:ProductsApiService, private _auth:AuthService) {
    this.auth = _auth
   }

  ngOnInit(): void { // Rx 
   
   this.api.loadList().subscribe(data =>  this.list = data)
  }

  login(){
    this.auth.login("","")
  }
  logout(){
    this.auth.logout()
  }
  createItem(){
    var fakeProduct:Product = new Product()
    fakeProduct.name = "Fake Product"
    fakeProduct.price = 0

    this.api.createItem(fakeProduct)
  }
}
