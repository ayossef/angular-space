import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/model/product.model';
import { ProductsApiService } from 'src/app/srv/products-api.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.sass']
})
export class DeleteComponent implements OnInit {

  id:number = 0
  item!:Product
  errMsg:string = ""
  constructor(private apiService: ProductsApiService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']
    // this.item = this.apiService.list[this.id]
    // this.route.params.subscribe(params => this.id = params['id'])
    this.apiService.loadItem(this.id).subscribe(product=>this.item = product)
  }
  delete(){
    console.log('Deleting item '+this.id)
    this.apiService.delete(this.id).subscribe(data=>{}, err => this.errMsg = JSON.stringify(err.message))
  }
}