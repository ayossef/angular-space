import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ListComponent } from './comp/list/list.component';
import { DetailsComponent } from './comp/details/details.component';

import { RouterModule, Routes } from '@angular/router';
import { DeleteComponent } from './comp/delete/delete.component';
import { DeleteGuardService } from './srv/delete-guard.service';

export const routes: Routes = [
  { path: 'list', pathMatch: 'full', component: ListComponent },
  { path: 'details/:id', pathMatch: 'full', component: DetailsComponent },
  { path: 'delete/:id', pathMatch: 'full', component: DeleteComponent, canActivate:[DeleteGuardService] },
]

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    DetailsComponent,
    DeleteComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
