import { Injectable } from '@angular/core';
import { AuthData } from '../auth-data.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentUser:AuthData = new AuthData()
  login(username:string, password:string):boolean{
    this.currentUser.isLoggedIn = true
    this.currentUser.username = ""
    return true
  }
  logout(){
    this.currentUser = new AuthData()   
  }
  isAllowedToAccess(path:string):boolean{ return false}
  constructor() { }
}
