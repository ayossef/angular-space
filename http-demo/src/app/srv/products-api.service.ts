import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../model/product.model';

@Injectable({
  providedIn: 'root'
})

export class ProductsApiService {
  list:Product[]=[]
  url:string = "http://localhost:4300/products/"
  httpOptions = {}
  constructor(private client:HttpClient) {
    this.initList()
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'your key here'
      })
    }
  }
  initList(){
    this.client.get<Product[]>(this.url).subscribe(data => this.list=data, err => console.log(err))
  }
  loadList():Observable<Product[]>{
    return this.client.get<Product[]>(this.url)
  }

  loadItem(id:number):Observable<Product>{
    return this.client.get<Product>(this.url+id)
  }

  createItem(item:Product){
    this.client.post(this.url,item,this.httpOptions).subscribe(
      data=> console.log('Item Creation Result:'+data),
    err => console.log('Creating data error:'+JSON.stringify(err)))
  }

  update(item:Product, id:number){
    this.client.put(this.url+id,item,this.httpOptions).subscribe(
      data=> console.log('Item Creation Result:'+data),
    err => console.log('Creating data error:'+JSON.stringify(err)))
  }

  delete(id:number){
    console.log('Calling deleted API for id:'+id)
    return this.client.delete(this.url+id)
  }
  // save(item:Product){
  //   if(item.id){
  //     this.createItem()
  //   }else {
  //     this.updateItem()
  //   }
  // }
}
