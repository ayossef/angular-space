import { TestBed } from '@angular/core/testing';

import { DeleteGuardService } from './delete-guard.service';

describe('DeleteGuardService', () => {
  let service: DeleteGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeleteGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
